//Benabdelali nabil
//Multec 1Ba (PDT student)
//2017-2018
//nabil.benabdelali@student.ehb.be
//Coördinates of cube
int middleX, middleY, top, bottom, left, right, topCorner, bottomCorner;

//Variables for myAnimation
int size = 10;
boolean playMyAnimation      = !false;
boolean playMyTweedeAnimatie = !false;
boolean playMyDerdeAnimatie  = !false;
boolean playMyVierdeAnimatie = !false;
boolean playMyVijfdeAnimatie = !false;
boolean playMyText           =  false;

//Variables for myTweedeAnimatie
int l = 130;
int m = 1100;

//Variables for myDerdeAnimation(MOUSE)
float i = 20;

//Variables for myVierdeAnimatie


//Variables for myVijfdeAnimatie
int j = 0;

//text
String b = "W: Leerkracht animatie";
String c = "x: Random ellipse";
String d = "c: Mouse,Left big, right again";
String e = "v: Random line";
String f = "b: Rotate random ellipse";
String o = "o: Press exit text animation to begin";
/*----------------------------*/



void setup() {
  size(1280, 720); 
  colorMode(HSB, 360, 100, 100, 100);
  //ellipseMode(CENTER);
  setGlobalVariables();
  initializeMinim();
}

void draw() {
  //White Cube planes
  background(0, 0, 100);
  //checks each frame if there is a beat.
  beat.detect(player.mix);

  drawAnimations();
  drawCubeMask();
  drawTweedeAnimatie();
  drawDerdeAnimatie();
  drawVierdeAnimatie();
  drawVijdeAnimatie();
  drawText();
}

void keyReleased() {
  //toggle Animations ON or OFF
  if (key == 'w') {
    playMyAnimation = !playMyAnimation; 
  } 
  if ( key == 'x') {
   playMyTweedeAnimatie = !playMyTweedeAnimatie;
  }
  if ( key == 'c') {
   playMyDerdeAnimatie = !playMyDerdeAnimatie;
   
  }
  if ( key == 'v') {
   playMyVierdeAnimatie = !playMyVierdeAnimatie;
   
  }
  if ( key == 'b') {
   playMyVijfdeAnimatie = !playMyVijfdeAnimatie;
   
  }
  if ( key == 'o'){
    playMyText = !playMyText;
  }
}
/*------------------eerste animatie-----------------------*/

void drawAnimations()
{  
  //play animation if toggled ON
  if (playMyAnimation == false) {
    myAnimation();
  }
}

void myAnimation() {
  if (beat.isOnset()) {
    size = 10;
  }
 
  noStroke();
  fill(frameCount % 360, 100, 100);
  ellipse(middleX, middleY, size, size);

  size+= 20;
}

/*------------------einde eerste animatie-----------------------*/


/*------------------tweede animatie-----------------------*/

void drawTweedeAnimatie()
{  
  //play animation if toggled ON
  if (playMyTweedeAnimatie == false) {
    myTweedeAnimatie();
  }
}

void myTweedeAnimatie(){
  
 
    stroke(frameCount%360, random(255), random(255));
    strokeWeight(4);
        for (int i = 0; i < 3; i++) {
        if ( beat.isOnset()) {
         size = 10;
        ellipse(l, 140, (frameCount * i) % 200, (frameCount * i ) % 200); //aantal frames * i % 200 
        ellipse(m, 140, (frameCount * i) % 200, (frameCount * i) % 200);  //aantal frames * i % 200 
        println(frameCount);//De systeemvariabele frameCount bevat het aantal frames dat is weergegeven sinds het programma is gestart. Binnen setup () is de waarde 0, na de eerste iteratie van draw is het 1,
      
    } else {
      ellipse(l, 140, 2, 2);
      ellipse(m, 140, 2, 2);
    }
  }
}
 
/*------------------einde tweede animatie mouse-----------------------*/


/*------------------derde animatie mouse-----------------------*/

void drawDerdeAnimatie()
{  
  //play animation if toggled ON
  if (playMyDerdeAnimatie == false) {
    myDerdeAnimatie();
  }
}
void myDerdeAnimatie(){
  
 
     stroke(frameCount%360, random(255), random(255));
     strokeWeight(2);
     
      if (mousePressed && beat.isOnset()) {
         size = 10;
         ellipse(mouseX,mouseY,i,i);
         i+=2;
      }else if(mouseButton == RIGHT ){
     
          i = 20;
        }
     }


/*------------------einde derde animatie-----------------------*/


/*------------------ Vierde animatie-----------------------*/

void drawVierdeAnimatie()
{  
  //play animation if toggled ON
  if (playMyVierdeAnimatie == false) {
    myVierdeAnimatie();
  }
}
void myVierdeAnimatie(){
      
      

      noFill();
      stroke(frameCount%360, random(255), random(255));
      strokeWeight(1);   
      
      
      
       for (int i = 0; i < 1500; i +=5) {
      if (beat.isOnset() ) {
        size = 10;
        line(i, random(240, 836), i, 840);
      } else {
        line(i, 836, i, 840);
      }
    }
  }
   
/*------------------einde vierde animatie-----------------------*/


/*------------------ Vijfde animatie-----------------------*/

void drawVijdeAnimatie()
{  
  //play animation if toggled ON
  if(playMyVijfdeAnimatie == false) {
    myVijdeAnimatie();
  }
}
void myVijdeAnimatie(){
  
   
   translate(width/2, height/2);
   noStroke();
   fill(frameCount%360, random(255), random(255));
    
    for (int i= 0; i <  1000; i++) {
    if (beat.isOnset() ) {
        size = 10;
        
      ellipse(i, i, 10, 10);
      rotate(j*-PI/200*0.1); //Positieve getallen roteren objecten met de klok mee en negatieve getallen roteren in de richting van de wijzers van de klok.
  

    } else{
      
      ellipse(i, i, 1, 1);
      rotate(j*-PI/200*0.1);//Positieve getallen roteren objecten met de klok mee en negatieve getallen roteren in de richting van de wijzers van de klok.
    } 
}
j++;
}

/*------------------einde vijde animatie-----------------------*/
/*------------------------TEXT-------------------------*/
void drawText(){
  
  //play animation if toggled ON
  if(playMyText == false) {
    myText();
  }
}
void myText(){
  
    
   fill(255);
   textSize(20);

   text(b,9,580);
   text(c,10,600);
   text(d,10,620);
   text(e,10,640);
   text(f,10,660);
   text(o,10,680);
}
/*------------------------TEXT-------------------------*/
/*------------------------taak is ready--------------*/
  